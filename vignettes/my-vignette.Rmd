---
title: "JEME"
author: "Christine Anyansi"
date: "`r Sys.Date()`"
output: BiocStyle::html_document
package: "`r pkg_ver('JEME')`"
keep_md: yes

vignette: >
  %\VignetteIndexEntry{JEME}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

This package helps users create and analyze the joint effect of multiple enhancers on their target genes for various types of data. Users can choose amongst the already available enhancer-target calls made for sample specific cases in Fantom5 or ROAADMAP. Or users can supplement their own enhancer-data for the calls to be generated with our algorithm. Once calls are made, the package also gives users the ability to analyze methylation, RNAseq, and mutation data to discover the effect of enhancer abherrations on their target genes and how it effects cancer. A  typical workflow is as follows

- Provide relevant data
- Generate enhancer-target pairs
- Analysis of enhancer-target pairs

This vignette will give a succinct overview of the JEME process. 

# Providing Data - The *jeme* object

All data is stored within a *jeme* object. This object can store:
- enhancer target pair calls
- gene expression data 
- methylation data 
- mutation data
- analysis results

Gene expression and methylation data must be supplied as either an matrix or a Summarized Experiment. If supplied as a matrix, rownames must be genes and columns must be the different samples. Along with a matrix, an additional data.frame containing the sample information must be provided. Row names must match the column names of the matrix, and the columns contain the sample information. The first column must indicate the differnt groups being tested (i.e. cancer vs noncancer), and if paired samples are being used, the second column must indicate the pairing information. Mutation data must be supplied as a dataframe or GRanges object in BED format. First column should be the mutation IDs and second column should be sample IDs. Enhancer-target pairs can be generated in multiple different ways which will be detailed in the section below. If no argument is given, it yields the default which includes all enhancer-target predictions in FANTOM5 samples. Assuming one has both cancer methylation and expression data and would like to see the broad impact of methylation in active enhancer regions using the default calls one would follow this process.

```{r, message = FALSE, warning = F}
library("JEME")

# generate random RNA expression data of 500 genes and 20 samples
genematrix <- matrix(rexp(10000, rate=.1), ncol=20)
rownames(genematrix) <- as.character(sample(3000,500,replace=F))
colnames(genematrix) <- 1:ncol(genematrix)

# generate random sample properties
sampleinformation <- data.frame(status = sample(0:1, 20, replace = T))
rownames(sampleinformation) <- colnames(genematrix)

# using sample methylation set in form of summarized experiment
methset <- setmet

myjeme <- makejeme(rnaexp = genematrix, meth = methset, sampinfo = sampleinformation )
myjeme

```

# Generate enhancer-target pairs

JEME offers four ways to generate enhancer-target pairs depending on how much data the user already has. 

## User has no data

In the case that the user has no data, the user can use our sample specific pre-generated enhancer-target network. This is done via the *activeEs* function. As default it outputs all enhancer-target pairs generated for FANTOM5. If a user is interested in the network generated for a specific samples, cancer types, tissues, or cell types from either FANTOM5 or ROADMAP it can be specified via this function. The output is defaulted to a GRAnges object but it can also be a jeme object. 


```{r, message = FALSE, warning = F}
pairings <- activeEs(fantom5 = T, tissue = "lung")
head(pairings)

```

## User already has data

In the case that the user already has their own data and wants to use our analysis pipeline, the user can supply this data to generate a *jeme* object using the *making_predictions_fantom5* function and setting the *choice* parameter to 1,2, or 3 depending on the type of predictions required. Similar to the case if one has no data, a jeme or GRAnges object can be made as an output.

- Choice 1: If the user already has their own enhancer-target pairs. Pairs must be supplied as a data frame with each row containing an enhancer region and a refseq gene ID. 
- Choice 2: If the user has enhancer regions, features, and gene expression values and wants a novel enhancer-target network. An enhancer matrix containing eRNA values and enhancer regions as rownames as well as a gene expression matrix containing unique gene refseq IDs with RNA seq values must be supplied. 
- Choice 3: Lastly, choice 3 can be chosen if the user only has specific enhancer regions they are interested in. The enhancer regions along with list of interested samples are used to generate enhancer-target network from our database. 


```{r}
# Choice 1 - User already has e-t pairs
# Generate (random) e-t pairs
pairs <- data.frame(enhancer = c("chr17:77965696-77966573", "chr11:118782911-118783378",
                                 "chr12:53267781-53268340", "chr12:53284889-53285178",
                                 "chr17:7738822-7740614"),
  geneid = c("NM_000152", "NM_000190", "NM_000224", "NM_000224", "NM_000546"))

out <- making_predictions_fantom5(choice= 1, pair= pairs)

# Choice 2 - User has enhancer and gene expression 

# generate enhancer expression data
enhancer <-  matrix(data = c(1.041711, 7.031550, 2.343850, 1.562567, 1.041711),
                    dimnames  = list(c("chr17:77965696-77966573", "chr11:118782911-118783378",
                                       "chr12:53267781-53268340", "chr12:53284889-53285178",
                                       "chr17:7738822-7740614"),
                                     "expression"))
# generate gene expression data
gene <-  matrix(data = c(0.0000000 , 0.0000000 , 2.7705245 , 0.5327932, 23.6204975),
                    dimnames  = list(c(c("NM_000152", "NM_000190", "NM_000224", "NM_000572",
                                         "NM_000546")),
                                     "expression"))

#out <- making_predictions_fantom5(choice = 2, enhancer= enhancer, gene = gene)
# Will result in no predictions

# Choice 3 - User only has enhancer regions and specific samples

enhancer <- data.frame(enhancer = c("chr17:77965696-77966573", "chr11:118782911-118783378",
                                 "chr12:53267781-53268340", "chr12:53284889-53285178",
                                 "chr17:7738822-7740614"))

samples <- c("CNhs10870","CNhs10620")

out <- making_predictions_fantom5(choice = 3, enhancerRegion = enhancer, samples = samples)
```


# Analyze Data

## Analysis
After the enhancer-target pairs have been generated further analysis can proceed by construction of a *jeme* object. If one wants to analyze the effect of enhancers active in pancreas samples with TCGA methylation and rnaseq data they first generate the enhancer-target pairs as shown in the previous section - specifying the tissue of interest, then make a *jeme* object to contain the methylation, rnaseq and enhancer-target calls.

```{r}
etpairspancreas <- activeEs(tissue = "pancreas")  
myjeme <- makejeme(rnaexp = setqet, meth = setmet, etpairs = etpairspancreas) 
```

With construction of the *jeme* object differential expression via the DESeq2 package can be conducted. 
```{r, message=FALSE}
jemeresults <- jemediffexpr(myjeme)
```

Differential methylation is conducted by first filtering methylation data then taking a t.test between enhancer regions under different conditions. Correction method and threshold for significance can be chosen by the user. 

```{r}
jemeresults <- filtersites(jemeresults)
jemeresults <- diffmeth(jemeresults)
jemeresults
```

## Results

Results can be accessed via the *resultstats* function.


```{r, include=FALSE}
   # add this chunk to end of mycode.rmd
  file.copy("~/JEME/vignettes/my-vignette.Rmd", "~/JEME/README.md")


```
