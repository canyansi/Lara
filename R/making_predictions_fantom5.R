#' Generate custom enhancer/target Network
#'
#' Choose a workflow based on the user-input data available
#'
#' @param choice A  choice of which prediction process wanted. Values in [1,4]
#' @param pair Enhancer-target pairs
#' @param enhancer Enhnacer regions with features
#' @param gene Gene IDs with expression level
#' @param thr1 The threshold of the region percentage of a user-input enhancer overlapping with the enhancers in our database, values in (0,1], default
#' 0.5
#' @param thr2 Random forest score threshold, values in (0,1], default 0.5
#' @param enhancerRegion Enhancer regions
#' @param samples A list of samples the user selects
#' @param makejeme Boolean indicating whether a jeme object should be returned
#' @return Enhancer-target pairs
#' @export
making_predictions_fantom5= function(choice,
                                     pair=NULL,
                                     enhancer=NULL,
                                     gene=NULL,
                                     thr1=0.5,
                                     thr2=0.5,
                                     enhancerRegion=NULL,
                                     samples=NULL,
                                     makejeme = F){

  #making sure there is proper arguments
  if (!hasArg(choice)) {
    stop("Please fill in 'choice' with which type of prediction process you'd like to be done
         1 - if you have your own enhancer-target pairss
         2 - if you have enhancer regions, features and gene expressions,
         and 3 - if you only provide enhancer regions")
  }

  if (choice == 1) {
    if (!hasArg(pair)) {
      stop ("For this type of process, you need to include the enhancer-target pairs. Column 1 should hold the enhancer regions and column 2 should hold the refseq code. For example:
            chr17:77965696-77966573   NM_000152
            ")
      }
  } else if (choice == 2){
    if (!(hasArg(enhancer) | hasArg(gene))) {
      stop ("For this type of process, you need to include the enhancer and gene expression levels.")
    }
  } else if (choice == 3) {
    if (!(hasArg(enhancerRegion) | hasArg(samples))) {
      stop ("For this type of process, you need to include the enhancerRegions and the sample names.")
    }
  }


  out <- switch (choice,
                 "1" = wrapper_enhancer_target_pairs_fantom5(pair),
                 "2" = wrapper_enhancer_regions_features_gene_expression_fantom5(enhancer,gene,thr1,thr2),
                 "3" = wrapper_enhancer_regions_only_fantom5(enhancerRegion,thr1,samples),
                 "4" = wrapper_nothing_fantom5(samples))

  if (nrow(out) == 0) {return ("No predictions, try changing parameters")}

  out$region <- as.character(out$enhancer)
  out$gene <- as.character(out$gene)
  out$chromosome <- sapply(strsplit(out$region, ":"), "[[", 1)
  startstop <- sapply(strsplit(out$region, ":"), "[[", 2)
  out$start <- as.numeric(sapply(strsplit(startstop, "-"), "[[",1))
  out$end <- as.numeric(sapply(strsplit(startstop, "-"), "[[",2))
  out$target <- sapply(strsplit(out$gene, "\\$"), "[[", 2)
  out$tss <- sapply(strsplit(out$gene, "\\$"), "[[", 4)
  out$strand <- sapply(strsplit(out$gene, "\\$"), "[[", 5)
  out$gene <- out$enhancer <- NULL

  if (choice == 2) {out$score <- NULL}
  out <- GRanges(out)
  metadata(out) <- list(from = "fantom5", query = "customchoice")

  if(makejeme){
    return (new("jeme", etpairs =out))
  }else{
    return (out)
  }

  return(out)
  }

