## Downloading cancer data from TCGA
# @importFrom TCGAbiolinks TCGAquery TCGAquery_samplesfilter TCGAquery_SampleTypes TCGAdownload
#  TCGAprepare
NULL
#' Query & Download TCGA data
#'
#' Downloads TCGA cancer data as summarized experiment. Expression data is downloaded
#'  as IlluminaHiSeq_RNASeqV2 if available. Methylation data is downloaded as 450k data.
#'
#' @param canctype Desired cancertype with TCGA IDs (i.e. PAAD, LIHC)
#' @param datatype Desired datatype. One of 'meth', 'rna', or 'both'.
#' @param paired Whether to search paired samples
#' @param dl Whether the files should be downloaded
#' @param Whether to save file  or not
#' @return Summarizedexperiment object containing the experimental information
#' @examples
#' jemedset = getdata("jemed", "both")
# export
getdata  = function(canctype, datatype, paired = T, dl = T, savefile = T){

  #pick data type
  dattype = switch(datatype,
                   "meth" = "humanmethylation450",
                   "rna" = "IlluminaHiSeq_RNASeqV2",
                   "both" = c("humanmethylation450", "IlluminaHiSeq_RNASeqV2"))

  #run query
  dquery <- TCGAquery(tumor = canctype, level = 3, platform = dattype)

  lsSample <- TCGAquery_samplesfilter(query = dquery)

  # Which samples are Primary Solid Tumor
  dataSmTP <- TCGAquery_SampleTypes(barcode = lsSample[[1]],
                                    typesample = c("TP", "TR", "TAP", "TB", "TRBM", "THOC", "TBM"))
  # Which samples are Solid Tissue Normal
  dataSmTN <- TCGAquery_SampleTypes(barcode = lsSample[[1]],
                                    typesample =c("NT", "NB", "NBM"))

  if (length(dataSmTN) == 0 | length(dataSmTP) == 0){
    stop(cat("There is no available normal or cancer data for this cancertype") )
  }

  if (paired){
    dataSmTN = dataSmTN[!duplicated(substring(dataSmTN, 1, 12))]
    dataSmTP = dataSmTP[!duplicated(substring(dataSmTP, 1, 12))]

    matchesn = substring(dataSmTN,1, 12) %in% substring(dataSmTP,1, 12)
    matchesp = substring(dataSmTP,1, 12) %in% substring(dataSmTN,1, 12)
    matches = c(dataSmTP[matchesp], dataSmTN[matchesn])
  }
  else {matches = c(dataSmTN,dataSmTP)}


  if (datatype == "both"){
    secondtypeTP =  TCGAquery_SampleTypes(barcode = lsSample[[2]],
                                          typesample = c("TP", "TR", "TAP", "TB", "TRBM", "THOC", "TBM"))

    secondtypeTN <- TCGAquery_SampleTypes(barcode = lsSample[[2]],
                                          typesample =c("NT", "NB", "NBM"))

    if (paired){
      secondtypeTN = secondtypeTN[!duplicated(substring(secondtypeTN, 1, 12))]
      secondtypeTP = secondtypeTP[!duplicated(substring(secondtypeTP, 1, 12))]


      matchesn = substring(secondtypeTN,1, 12) %in% substring(secondtypeTP,1, 12)
      matchesp = substring(secondtypeTP,1, 12) %in% substring(secondtypeTN,1, 12)

      matchestwo = c(secondtypeTP[matchesp], secondtypeTN[matchesn])

      matchestwo = matchestwo[substring(matchestwo, 1, 12) %in% substring(matches, 1, 12)]
      matches = matches[substring(matches, 1, 12) %in% substring(matchestwo, 1, 12)]

      finalmatches = list(matches, matchestwo)
    }
    else{finalmatches = list(matches, c(secondtypeTN, secondtypeTP))}
  }
  else{finalmatches = matches}

  #downloading
  if (dl){
    cat ("downloading")
    if (!datatype == "both"){
      typee = switch(datatype,
                     "meth" = NULL,
                     "rna" = "rsem.genes.results")

      TCGAdownload(dquery, path = datatype, type = typee,
                   samples = finalmatches)
    }
    else{
      TCGAdownload(dquery, path = "meth",samples = finalmatches[[1]])
      TCGAdownload(dquery, path = "rna", type = "rsem.genes.results",
                   samples = finalmatches[[2]])
    }

    cat ("reading in files")
    if(datatype == "both"){

      metq = dquery[dquery$Platform == "HumanMethylation450",]
      rnaq = dquery[dquery$Platform == "IlluminaHiSeq_RNASeqV2",]

      methset <- TCGAprepare(metq, dir = "meth",
                             save = F,
                             samples = finalmatches[[1]],
                             filename = paste(canctype, "meth.rda", sep = "_"))

      exprset <- TCGAprepare(rnaq, dir = "rna",
                             save = F,
                             samples = finalmatches[[2]],
                             type = "rsem.genes.results",
                             filename = paste(canctype, "expr.rda", sep = "_"))


      colData(exprset) <- colData(exprset)[,c(4,1)]
      colData(methset) <- colData(methset)[,c(4,1)]

      exprset <- exprset[!duplicated(exprset),]
      methset <- methset[!duplicated(methset),]


      if (savefile){
        save(exprset, file = paste(canctype, "_expr.rda", sep = "") )
        save(methset, file = paste(canctype, "_meth.rda", sep = "") )

      }


      set = list(methset,exprset)
    }
    else{
      if (datatype == "meth"){
        set <- TCGAprepare(dquery, dir = "meth",
                           save = F,
                           samples = finalmatches)
        colData(set) <- colData(set)[,c(4,1)]
        set <- set[!duplicated(set),]
        if (savefile){save(set, file = paste(canctype, "_meth.rda", sep = "") )}
      }
      else {
        set <- TCGAprepare(dquery, dir = "rna",
                           save = F,
                           type = "rsem.genes.results",
                           samples = finalmatches)
        colData(set) <- colData(set)[,c(4,1)]
        set <- set[!duplicated(set),]
        if (savefile){save(set, file = paste(canctype, "_expr.rda", sep = "") )}
      }
    }
    return (set)
  }
}

#' Read Downloaded TCGA data
#'
#' Reads and processes downloaded TCGA data into Summarized Experiment
#' @param directory Should be the folder that holds the folder containing the TCGA data
#' @param canctype The cancertype. One of 'meth', 'rna', or 'both'.
#' @param datatype The type of data
#' @param savefile Whether the data should be saved into a folder
#' @param namefile The name of the savefile
#' @examples
#' set2 = readdata("rna", "PAAD", "rna", savefile = F, namefile = "newfile")
# @export
readdata = function(directory, canctype, datatype, savefile = T, namefile){

  dattype = switch(datatype,
                   "meth" = "humanmethylation450",
                   "rna" = "IlluminaHiSeq_RNASeqV2")

  if (!hasArg("namefile") ){
    namefile = paste(canctype, datatype, sep = "")
  }

  query <- TCGAquery(tumor = canctype, level = 3, platform = dattype )

  typee = switch(datatype,
                 "meth" = NULL,
                 "rna" = "rsem.genes.results")

  set <- TCGAprepare(query, dir = directory,
                     type = typee,
                     filename = namefile)
  return(set)
}


