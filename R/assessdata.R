#' @include makingobject.R
#' @include rnaanal.R
#' @include methanal.R
NULL
# Reshaping and Plotting -------------------------------------------------

#' Melt a Paa Object
#'
#' This function melts  a jeme object
#' @param obj A jeme object
#' @return A melted dataframe containing rna , methylation or both sets
#' @examples
#' newjeme <- makejeme(meth = setmet)
#' melted <- meltjeme(newjeme)
#' @export
meltjeme <- function(obj){
  if(class(obj) != "jeme"){
    stop ("Please submit a jeme object for analysis")
  }

  if (nrow(obj@exprinfo) > 0){
    tomeltrna <- obj@exprinfo
    rna = T
    meltrna <- data.table::data.table(assay(tomeltrna), keep.rownames = T)
    names(meltrna)[1] <- "gene"
    geneids <- c("gene", "type")
    mcols(tomeltrna)$type <- "gene"
    meltrna[,"type"] <-  data.table::as.data.table(mcols(tomeltrna)["type"])
    meltrna <- data.table::melt(meltrna, id.vars = geneids)

    if ("sample" %in% names(colData(tomeltrna))){
      meltrna$sample <- colData(tomeltrna)[meltrna$variable, "sample"]
    }
    if ("condition" %in% names(colData(tomeltrna))){
      meltrna$condition <- colData(tomeltrna)[meltrna$variable, "condition"]
    }
  } else{
    rna = F
  }

  if ("origsites" %in% names(obj@results)){
    tomelt <- obj@results$origsites
    meth = T
  } else if (nrow(obj@meth) > 0){
    tomelt <- obj@meth
    meth = T
  } else{
    meth = F
  }

  if (!(meth | rna)){
    stop ("To melt jeme object, need to have methylation or rna data.")
  }

  if (meth){
    meltdf <- data.table::data.table(assay(tomelt), keep.rownames = T)
    names(meltdf)[1] <- "probe"
    ids <- "probe"
    if ("region" %in% names(mcols(tomelt))){
      ids <- c(ids,"region")
    }
    if (!"type" %in% names(mcols(tomelt))){
      mcols(tomelt)$type <- "methylation"
    }
    ids <- c(ids, "type")

    meltdf[,ids] <-  data.table::as.data.table(mcols(tomelt)[ids])

    meltdf <- data.table::melt(meltdf, id.vars = ids)


    if ("sample" %in% names(colData(tomelt))){
      meltdf$sample <- colData(tomelt)[meltdf$variable, "sample"]
    }
    if ("condition" %in% names(colData(tomelt))){
      meltdf$condition <- colData(tomelt)[meltdf$variable, "condition"]
    }
  }

  if (meth & rna){
    names(meltdf)[names(meltdf) == "probe"] =
      names(meltrna)[names(meltrna) == "gene"] = "ID"
    meltdf <- rbind(meltdf, meltrna, fill = T)
  } else if (rna){
    meltdf <- meltrna
  }

  names(meltdf)[names(meltdf) == "variable"] = "samplename"

  return(meltdf)
}


#' function internal
#' @keywords internal
minus <- function(y) {return (y[1] - y[2])}

#' Plot difference per site per sample (paired)
#'
#' Makes heatmap of per sample per site differences. GGplot is needed to make the plot
#' @param object A Paa or a melted dataframe object (returned from meltjeme)
#' @param ids Vector of ids for which to subset by. If none supplied will use the first
#'  five ids
#' @param by Whether plot should be by region (ids, genenames, etc) or by type (genes vs
#'  enhancers)
#' @return list containing ggplot and data used to plot
#' @examples
#' newjeme <- makejeme(meth = setmet)
#' jemeres <- filtersites(newjeme)
#' plot <- sitediff(jemeres)
#' @export
sitediff <- function (object, ids, by = "region"){

  #checking which type of object it is.
  if(!(inherits(object, "data.frame") | inherits(object, "jeme"))){
    stop ("object needs to be an already melted dataframe type object or a jeme object")
  } else if (inherits(object, "jeme")){
    if (hasrna(object)) object@exprinfo <- pairedcheck(object@exprinfo, paired = T)
    if (hasmeth(object)) object@meth <- pairedcheck(object@meth, paired = T)
    meltdf <- meltjeme(object)
  } else{
    meltdf <- object
  }

  if (!any(c("probe", "gene", "ID") %in% names (meltdf)) ){
    stop ("dataframe needs to be melted by the function meltjeme first")
  } else{
    names(meltdf)[names(meltdf) %in% c("probe", "gene", "ID")] = "ID"
  }

  if (hasArg(ids)){
    suppressWarnings(if (ids == "all") ids <- unique(meltdf$ID))
    if(length(intersect(ids, meltdf$region)) > 0){
      meltdf = meltdf[region %in% ids]
    } else if (length(intersect(ids, meltdf$ID)) > 0){
      meltdf = meltdf[ID %in% ids]
    } else {
      warning("None of your supplied ids are available - using first 5 sites")
      sub = unique(meltdf$ID)[1:5]
      meltdf = meltdf[ID %in% sub,]
    }
  } else{
    sub = unique(meltdf$ID)[1:5]
    meltdf = meltdf[ID %in% sub,]
    message("No regions given, using first IDs")
  }


  meltdf <- as.data.table(meltdf)

  #log 2 of pseudocount of gene
  meltdf[type == "gene", value := log2(value + 1)]

  if(!all(c("sample", "condition") %in% names(meltdf))){
    stop ("This type of analysis requires paired samples.")
  } else{
    meltdf <- meltdf[!is.na(sample),]
  }

  meltdf <- meltdf[order(meltdf$sample, meltdf$condition, meltdf$ID),]

  if(any(duplicated(meltdf))){
    message("Removing ",length(which(duplicated(meltdf))), " duplicates")
    meltdf <- meltdf[!duplicated(meltdf),]
  }

  if ("region" %in% names(meltdf)){
    meltdf[is.na(region), region := ID]
    deltasite <- meltdf[,.(diff = minus(value)), by = list(ID, sample, region)]
    #deltasite[,region := meltdf$region[match(deltasite$ID, meltdf$ID)]]
    deltasite[is.na(region), region:= ID]
  } else{
    deltasite <- meltdf[,.(diff = minus(value)), by = list(ID, sample)]
    deltasite <- meltdf[,.(diff = minus(value)), by = list(ID, sample)]
  }

  if (by == "type"){
    byy = ".~type"
    deltasite[,type := meltdf$type[match(deltasite$ID, meltdf$ID)]]
  } else if (!("region" %in% names(deltasite))){
    byy = ".~ID"
  } else{
    byy = ".~region"
  }

  tt = bquote(paste(Delta, .(levels(meltdf$condition)[2]), ":", .(levels(meltdf$condition)[1])))

  plot <- ggplot2::ggplot(data = deltasite, mapping = ggplot2::aes(ID, sample)) +
    ggplot2::geom_tile(ggplot2::aes(fill = diff), colour = "white") +
    ggplot2::facet_grid(facets = byy,scales = "free_x", space = "free_x") +
    ggplot2::scale_fill_gradient2(low ="steelblue4",mid = "white",
                                  high = "firebrick3", midpoint = 0)
  plot <- plot +  ggplot2::ggtitle(tt)

  return (list(plot = plot, data = as.data.frame(deltasite)))
}

